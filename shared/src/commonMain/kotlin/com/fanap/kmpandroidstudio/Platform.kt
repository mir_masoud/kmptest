package com.fanap.kmpandroidstudio

interface Platform {
    val name: String
}

expect fun getPlatform(): Platform